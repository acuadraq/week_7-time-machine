import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import FourByFour from './components/FourByFour/FourByFour';
import Navbar from './components/Navbar';
import TicTacToe from './components/TicTacToe/TicTacToe';

const App = () => {
  return (
    <Router>
      <Navbar />
      <section>
        <div className="container">
          <Switch>
            <Route exact path="/" component={FourByFour} />
            <Route path="/tictactoe" component={TicTacToe} />
          </Switch>
        </div>
      </section>
    </Router>
  );
};

export default App;
