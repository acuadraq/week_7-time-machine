import { useEffect, useState } from 'react';

const usePrevious = (value: number) => {
  const [arr, setArr] = useState<number[]>([]);

  useEffect(() => {
    if (value === 0 || value === arr[0]) {
      return;
    }
    setArr([value, ...arr]);
  }, [value]);

  return arr;
};

export default usePrevious;
