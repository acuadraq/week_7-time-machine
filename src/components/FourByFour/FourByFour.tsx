import React, { useState } from 'react';
import usePrevious from '../../customhooks/usePrevious';
import Board from './Board';

const FourByFour = () => {
  const [position, setPosition] = useState<number>(0);
  const [arrayNumbers, setArrayNumbers] = useState<number>(0);
  const [arrayPosition, setArrayPosition] = useState<number>(0);
  const previous: number[] = usePrevious(arrayNumbers);

  const handleClick = (id: number) => {
    if (arrayPosition !== 0) return;
    setPosition(id);
    setArrayNumbers(id);
  };

  const goNext = () => {
    const nextStep: number = arrayPosition - 1;
    if (nextStep < 0) return;
    setArrayPosition(nextStep);
    setPosition(previous[nextStep]);
  };

  const goBack = () => {
    const backStep: number = arrayPosition + 1;
    if (previous[backStep] === undefined) return;
    setArrayPosition(backStep);
    setPosition(previous[backStep]);
  };

  const resume = () => {
    if (arrayPosition === 0) return;
    setArrayPosition(0);
    setPosition(previous[0]);
  };

  return (
    <div className="game">
      <div className="game__container">
        <Board handleClick={handleClick} position={position} />
        <div className="game-options">
          <button
            onClick={goNext}
            type="button"
            className={arrayPosition - 1 < 0 ? 'button--disabled' : 'button--active'}
          >
            Next
          </button>
          <button
            type="button"
            className={
              previous[arrayPosition + 1] === undefined ? 'button--disabled' : 'button--active'
            }
            onClick={goBack}
          >
            Previous
          </button>
          <button type="button" className="button--active" onClick={resume}>
            Resume
          </button>
        </div>
      </div>
    </div>
  );
};

export default FourByFour;
