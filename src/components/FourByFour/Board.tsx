import React from 'react';
import Square from './Square';

type BoardTypes = {
  handleClick: (id: number) => void;
  position: number;
};

const Board = ({ handleClick, position }: BoardTypes) => {
  const array: number[] = Array.from(Array(16).keys());

  return (
    <div className="container__board">
      <div className="board__four">
        {array.map(num => (
          <Square key={num} id={num + 1} handleClick={handleClick} position={position} />
        ))}
      </div>
    </div>
  );
};

export default Board;
