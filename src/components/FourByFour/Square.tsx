import React from 'react';

type SquareVar = {
  id: number;
  handleClick: (id: number) => void;
  position: number;
};

const Square = ({ id, handleClick, position }: SquareVar) => {
  const classForButtons = `class-${id.toString()} opacity--down ${
    position === id ? 'selected' : ''
  }`;
  return (
    <button type="button" className={classForButtons} onClick={() => handleClick(id)}>
      {' '}
    </button>
  );
};

export default Square;
