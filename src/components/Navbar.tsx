import React from 'react';
import { Link } from 'react-router-dom';

const Navbar = () => {
  return (
    <header>
      <nav className="flex-class">
        <ul className="flex-class">
          <li className="navbar__title">
            <Link to="/">Navigation</Link>
          </li>
          <div>
            <li>
              <Link to="/">FourByFour</Link>
            </li>
            <li>
              <Link to="/tictactoe">Tic Tac Toe</Link>
            </li>
          </div>
        </ul>
      </nav>
    </header>
  );
};

export default Navbar;
