import React, { useState } from 'react';
import calculateWinner from '../../helpers/winner';
import Square from './Square';

type arrays = {
  squares: string[] | null[];
  handleClick: (id: number) => void;
};

const Board = ({ squares, handleClick }: arrays) => {
  const arr: number[] = Array.from(Array(9).keys());

  return (
    <div className="board__grid">
      {arr.map(num => (
        <Square key={num} value={squares[num]} id={num} handleClick={handleClick} />
      ))}
    </div>
  );
};
export default Board;
