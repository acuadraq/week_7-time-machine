import React, { useState } from 'react';

type props = {
  value: string | null;
  id: number;
  handleClick: (id: number) => void;
};

const Square = ({ value, id, handleClick }: props) => {
  return (
    <button type="button" className="square" onClick={() => handleClick(id)}>
      {value}
    </button>
  );
};
export default Square;
