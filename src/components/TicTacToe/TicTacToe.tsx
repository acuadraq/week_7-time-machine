/* eslint-disable react/jsx-one-expression-per-line */
/* eslint-disable operator-linebreak */
/* eslint-disable no-shadow */
import React, { ReactElement, useState } from 'react';
import calculateWinner from '../../helpers/winner';
import Board from './Board';

type arrays = {
  squares: string[] | null[];
};

const TicTacToe = () => {
  const [history, sethistory] = useState<arrays[]>([{ squares: Array(9).fill(null) }]);
  const [xIsNext, setXIsNext] = useState<boolean>(true);
  const [stepNumber, setStepNumber] = useState<number>(0);
  const [replay, setReplay] = useState<boolean>(false);
  const current: arrays = history[stepNumber];
  const winner: string | null = calculateWinner(current.squares);

  const handleClick = (i: number) => {
    const prevhistory: arrays[] = history.slice(0, stepNumber + 1);
    const currentarr: arrays = prevhistory[prevhistory.length - 1];
    const square: string[] | null[] = currentarr.squares.slice();
    if (calculateWinner(square) || square[i] || replay) {
      return;
    }
    square[i] = xIsNext ? 'X' : 'O';
    sethistory(prevhistory.concat([{ squares: square }]));
    setStepNumber(prevhistory.length);
    setXIsNext(!xIsNext);
  };

  const restartGame = () => {
    if (replay) return;
    setStepNumber(0);
    sethistory([{ squares: Array(9).fill(null) }]);
    setXIsNext(true);
  };

  const goBack = () => {
    const backStep: number = stepNumber - 1;
    if (backStep < 0 || replay) return;
    setXIsNext(backStep % 2 === 0);
    setStepNumber(backStep);
  };

  const resumeGame = () => {
    const newLength: number = history.length - 1;
    if (replay) return;
    setXIsNext(newLength % 2 === 0);
    setStepNumber(newLength);
  };

  const goForward = () => {
    const newStep: number = stepNumber + 1;
    if (newStep > history.length - 1 || replay) return;
    setXIsNext(newStep % 2 === 0);
    setStepNumber(newStep);
  };

  const replayGame = () => {
    setReplay(true);
    let time: number = 0;
    const interval = setInterval(() => {
      if (time <= stepNumber) {
        setStepNumber(time);
        // eslint-disable-next-line no-plusplus
        time++;
      } else {
        setReplay(false);
        clearInterval(interval);
      }
    }, 500);
  };
  let modal!: ReactElement;
  if (winner || stepNumber === 9) {
    modal = (
      <div className="modal">
        <div className="modal-content">{winner ? <p>Winner is {winner}</p> : <p>Its a Tie</p>}</div>
      </div>
    );
  }

  return (
    <div className="game">
      <div className="game__container">
        <div className="grid-left__side">
          {modal}
          <div className="board__container">
            <Board squares={current.squares} handleClick={handleClick} />
          </div>
        </div>
        <div className="game-options">
          <button
            type="button"
            onClick={goForward}
            className={stepNumber + 2 > history.length ? 'button--disabled' : 'button--active'}
          >
            Next
          </button>
          {winner || stepNumber === 9 ? (
            <button type="button" className="button--active" onClick={replayGame}>
              Replay
            </button>
          ) : (
            <button
              type="button"
              className={stepNumber + 2 > history.length ? 'button--disabled' : 'button--active'}
              onClick={resumeGame}
            >
              Resume
            </button>
          )}
          <button
            type="button"
            className={stepNumber - 1 < 0 ? 'button--disabled' : 'button--active'}
            onClick={goBack}
          >
            Previous
          </button>
          <button type="button" onClick={restartGame} className="button--active">
            Restart
          </button>
          <div>
            <p>Next to move</p>
            <div className="next-move">{xIsNext ? 'X' : 'O'}</div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default TicTacToe;
