# Week_7 Time Machine

![](public/Screenshot.png)

## Description

This is the weekly assignment for the seventh week. Its a time machine web. This are the main points of the page:

- This website is using React and Typescript
- Use of react router dome for the site pages and links.
- It has two pages one for the Four By Four game and the other one for the tic tac toe

Four By Four Points:

- It has a custom hook to save every new movement made in a array.
- You can go back in time to see the old movements.
- You can go forward to see the last movementes.
- You can not make new movements when you are traveling.
- You can resume the game when you are traveling

Tic Tac Toe Points:

- Records every movemente of the game until a winner is determined or is a tie.
- You can replay the game once you have finished the game (and you can not click anything on that time).
- You can go back and forward with the buttons.
- You can restart the game at anytime.
- The UI tells you who is next.

If you want to tested in real time use this url: https://week-7-time-machine.vercel.app/
